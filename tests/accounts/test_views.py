"""Test datawarehouse.accounts.views."""
import unittest
from unittest import mock

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from freezegun import freeze_time

from datawarehouse.utils import timestamp_to_datetime
from tests import utils

User = get_user_model()


class UserGetTest(utils.TestCase):
    """Test user_get."""

    fixtures = [
        'tests/accounts/fixtures/basic.yaml',
    ]

    def test_get(self):
        """Test getting user details."""
        user = User.objects.get(username='loyal_user')
        response = self.client.get('/user/loyal_user')
        self.assertContextEqual(
            response.context,
            {'queried_user': user}
        )


class UserSettingsTest(utils.TestCase):
    """Test user_settings."""

    fixtures = [
        'tests/accounts/fixtures/basic.yaml',
    ]

    def setUp(self):
        """SetUp."""
        self.user = User.objects.get(username='loyal_user')
        self.auth_client = self.get_authenticated_client(self.user)

    def test_get(self):
        """Test getting user details."""
        response = self.auth_client.get('/user/-/settings')
        self.assertContextEqual(
            response.context,
            {'user': self.user}
        )

    def test_edit(self):
        """Test modifying user details."""
        post_args = ('/user/-/settings',
                     {'name': 'New name', 'username': 'newname'})
        post_kwargs = {
            'redirect_login': True,
            'redirect_url': '/user/-/login/?next=/user/-/settings',
            'user': self.user,
        }
        self.assert_authenticated_post(200, [], *post_args, **post_kwargs)

        user = User.objects.get(username='newname')
        self.assertEqual(self.user.id, user.id)
        self.assertEqual('New name', user.first_name)
        self.assertEqual('newname', user.username)

    def test_edit_taken_username(self):
        """Test modifying user details. Username already taken."""
        User.objects.create(username='great_username')
        post_args = ('/user/-/settings',
                     {'name': 'New name', 'username': 'great_username'})
        post_kwargs = {
            'redirect_login': True,
            'redirect_url': '/user/-/login/?next=/user/-/settings',
            'user': self.user,
        }
        response = self.assert_authenticated_post(200, [], *post_args, **post_kwargs)

        self.assertContextEqual(
            response.context,
            {'error': 'Username already taken'}
        )

        # Check that it didnt change anything.
        user = User.objects.get(username='loyal_user')
        self.assertEqual(self.user.id, user.id)
        self.assertEqual('Loyal User', user.first_name)
        self.assertEqual('loyal_user', user.username)

    def test_edit_missing_fields(self):
        """Test modifying user details. Missing fields."""
        User.objects.create(username='great_username')
        post_args = ('/user/-/settings',
                     {'name': 'New name'})
        post_kwargs = {
            'redirect_login': True,
            'redirect_url': '/user/-/login/?next=/user/-/settings',
            'user': self.user,
        }
        response = self.assert_authenticated_post(200, [], *post_args, **post_kwargs)

        self.assertContextEqual(
            response.context,
            {'error': 'Missing fields'}
        )

        # Check that it didnt change anything.
        user = User.objects.get(username='loyal_user')
        self.assertEqual(self.user.id, user.id)
        self.assertEqual('Loyal User', user.first_name)
        self.assertEqual('loyal_user', user.username)

    def test_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['get', 'post']
        url = '/user/-/settings'
        client = self.get_authenticated_client(self.user)

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            response = getattr(client, method)(url)
            self.assertEqual(405, response.status_code, method)


class UserDeleteTest(utils.TestCase):
    """Test user_delete."""

    fixtures = [
        'tests/accounts/fixtures/basic.yaml',
    ]

    def setUp(self):
        """SetUp."""
        self.user = User.objects.get(username='loyal_user')
        self.auth_client = self.get_authenticated_client(self.user)

    def test_get(self):
        """Test getting delete view does nothing."""
        response = self.auth_client.get('/user/-/delete')
        self.assertContextEqual(
            response.context,
            {'user': self.user}
        )
        self.assertTrue(User.objects.filter(id=self.user.id).exists())

    @mock.patch('datawarehouse.utils.notify_user')
    def test_delete(self, notify_user):
        """Test deleting user."""
        post_args = ('/user/-/delete', {})
        post_kwargs = {
            'redirect_login': True,
            'redirect_url': '/user/-/login/?next=/user/-/delete',
            'user': self.user,
        }
        response = self.assert_authenticated_post(302, [], *post_args, **post_kwargs)
        self.assertRedirects(response, '/user/-/login/')
        self.assertFalse(User.objects.filter(id=self.user.id).exists())

        # The email was sent
        self.assertTrue(notify_user.called)

        # Check the email content
        email_body = (
            loader.get_template('registration/user_delete_email.html')
            .render({
                'user': self.user,
                'domain': 'testserver',
                'protocol': 'http',
            }, response.context)
        )

        notify_user.assert_called_with(
            subject='Your account was successfully deleted', message=email_body, user=mock.ANY
        )
        # Test user separately because the call param is SimpleLazyObject(User), not User
        self.assertEqual(
            notify_user.call_args_list[0][1]['user'].username,
            self.user.username,
        )

    def test_email_signup(self):
        """Test deleting user. Signup enabled."""
        email_body = (
            loader.get_template('registration/user_delete_email.html')
            .render({
                'user': self.user,
                'domain': 'testserver',
                'protocol': 'http',
                'FF_SIGNUP_ENABLED': True,
            })
        )
        self.assertTrue('sign up again' in email_body)

    def test_email_no_signup(self):
        """Test deleting user. No signup enabled."""
        email_body = (
            loader.get_template('registration/user_delete_email.html')
            .render({
                'user': self.user,
                'domain': 'testserver',
                'protocol': 'http',
                'FF_SIGNUP_ENABLED': False,
            })
        )
        self.assertFalse('sign up again' in email_body)

    def test_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['get', 'post']
        url = '/user/-/delete'
        client = self.get_authenticated_client(self.user)

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            response = getattr(client, method)(url)
            self.assertEqual(405, response.status_code, method)


@unittest.skipUnless(settings.FF_SIGNUP_ENABLED, 'Signup Disabled')
class UserSignupTest(utils.TestCase):
    """Test user_signup."""

    fixtures = [
        'tests/accounts/fixtures/basic.yaml',
    ]

    def test_get(self):
        """Test getting create view."""
        response = self.client.get('/user/-/signup')
        self.assertEqual(200, response.status_code)

    @mock.patch('datawarehouse.accounts.views.default_token_generator.make_token')
    @mock.patch('datawarehouse.utils.notify_user')
    def test_create(self, notify_user, make_token):
        """Test creating user."""
        make_token.return_value = 'token123'
        post_args = (
            '/user/-/signup',
            {
                'username': 'user_name',
                'email': 'user@email.com',
                'name': 'User Name',
                'captcha_answer': 'passed'
            }
        )
        self.client.post(*post_args)

        # If the user was not created this should fail.
        user = User.objects.get(
            username='user_name',
            email='user@email.com',
            first_name='User Name'
        )

        # The email was sent
        self.assertTrue(notify_user.called)

        # Check the email content
        email_body = (
            loader.get_template('registration/user_signup_email.html')
            .render({
                'token': 'token123',
                'uid': urlsafe_base64_encode(force_bytes(user.id)),
                'user': user,
                'domain': 'testserver',
                'protocol': 'http',
            })
        )

        notify_user.assert_called_with(
            subject='Confirm your account', message=email_body, user=user
        )

    @mock.patch('datawarehouse.accounts.views.loader.get_template')
    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_create_token(self, send_mail, get_template):
        """Test create user generates a correct token for confirming creation."""
        template_mock = mock.Mock()
        get_template.return_value = template_mock

        post_args = (
            '/user/-/signup',
            {
                'username': 'user_name',
                'email': 'user@email.com',
                'name': 'User Name',
                'captcha_answer': 'passed'
            }
        )
        self.client.post(*post_args)

        # Check token
        self.assertTrue(
            default_token_generator.check_token(
                User.objects.get(username='user_name'),
                template_mock.render.call_args.args[0]['token']
            )
        )

    def test_create_missing_fields(self):
        """Test creating user. Missing fields."""
        post_args = (
            '/user/-/signup',
            {
                'username': 'user_name',
                'email': 'user@email.com',
                'captcha_answer': 'passed'
            }
        )
        response = self.client.post(*post_args)

        self.assertContextEqual(
            response.context,
            {
                'username': 'user_name',
                'email': 'user@email.com',
                'name': None,
                'error': 'Missing fields'
            }
        )

        self.assertFalse(User.objects.filter(username='user_name').exists())

    def test_create_username_taken(self):
        """Test creating user. Username not available."""
        post_args = (
            '/user/-/signup',
            {
                'username': 'loyal_user',
                'email': 'user@email.com',
                'name': 'User Name',
                'captcha_answer': 'passed'
            }
        )
        response = self.client.post(*post_args)

        self.assertContextEqual(
            response.context,
            {
                'username': 'loyal_user',
                'email': 'user@email.com',
                'name': 'User Name',
                'error': 'Username already taken',
                'error_input': 'username'
            }
        )

    def test_create_email_in_use(self):
        """Test creating user. Email in use."""
        post_args = (
            '/user/-/signup',
            {
                'username': 'user_name',
                'email': 'loyal_user@email.com',
                'name': 'User Name',
                'captcha_answer': 'passed'
            }
        )
        response = self.client.post(*post_args)

        self.assertContextEqual(
            response.context,
            {
                'username': 'user_name',
                'email': 'loyal_user@email.com',
                'name': 'User Name',
                'error': 'Email address already in use',
                'error_input': 'email'
            }
        )

        self.assertFalse(User.objects.filter(username='user_name').exists())

    def test_invalid_captcha(self):
        """Test creating user. Invalid."""
        post_args = (
            '/user/-/signup',
            {
                'username': 'user_name',
                'email': 'user@email.com',
                'name': 'User Name',
                'captcha_answer': 'Invalid'
            }
        )
        response = self.client.post(*post_args)

        self.assertContextEqual(
            response.context,
            {
                'username': 'user_name',
                'email': 'user@email.com',
                'name': 'User Name',
                'error': 'Incorrect Captcha solution',
                'error_input': 'captcha'
            }
        )

        self.assertFalse(User.objects.filter(username='user_name').exists())

    def test_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['get', 'post']
        url = '/user/-/signup'

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            response = getattr(self.client, method)(url)
            self.assertEqual(405, response.status_code, method)


@unittest.skipUnless(settings.FF_SIGNUP_ENABLED, 'Signup Disabled')
class UserSignupDoneTest(utils.TestCase):
    """Test user_signup_done."""

    def test_get(self):
        """Test getting signup done view."""
        response = self.client.get('/user/-/signup/done')
        self.assertEqual(200, response.status_code)

    def test_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['get']
        url = '/user/-/signup/done'

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            response = getattr(self.client, method)(url)
            self.assertEqual(405, response.status_code, method)


@unittest.skipUnless(settings.FF_SIGNUP_ENABLED, 'Signup Disabled')
class UserSignupCompleteTest(utils.TestCase):
    """Test user_signup_complete."""

    fixtures = [
        'tests/accounts/fixtures/basic.yaml',
    ]

    def setUp(self):
        """SetUp."""
        self.user = User.objects.get(username='loyal_user')
        self.uid = urlsafe_base64_encode(force_bytes(self.user.id))
        self.token = default_token_generator.make_token(self.user)

    def test_get(self):
        """Test getting view."""
        response = self.client.get(f'/user/-/signup/complete/{self.uid}/{self.token}')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(
            response.context,
            {'uidb64': self.uid, 'token': self.token}
        )
        self.assertEqual(None, response.context.get('error'))

    def test_get_wrong_token(self):
        """Test getting view with wrong token."""
        response = self.client.get(f'/user/-/signup/complete/{self.uid}/foobar')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(
            response.context,
            {'error': 'Token is not valid. If the problem persists contact an administrator.'}
        )

    def test_post_wrong_token(self):
        """Test post view with wrong token."""
        response = self.client.post(f'/user/-/signup/complete/{self.uid}/foobar')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(
            response.context,
            {'error': 'Token is not valid. If the problem persists contact an administrator.'}
        )

    def test_post(self):
        """Test post view changes password."""
        post_args = (
            f'/user/-/signup/complete/{self.uid}/{self.token}',
            {'password1': 'superstrongpassword', 'password2': 'superstrongpassword'}
        )
        self.assertFalse(self.user.check_password('superstrongpassword'))
        response = self.client.post(*post_args)
        self.assertRedirects(response, '/user/-/login/')

        user = User.objects.get(username='loyal_user')
        self.assertTrue(user.check_password('superstrongpassword'))

    def test_post_different_passwords(self):
        """Test post view with different passwords."""
        post_args = (
            f'/user/-/signup/complete/{self.uid}/{self.token}',
            {'password1': 'superstrongpassword', 'password2': 'foobar'}
        )
        response = self.client.post(*post_args)
        self.assertEqual(200, response.status_code)
        self.assertEqual('Passwords do not match', response.context.get('error'))

    def test_post_missing_fields(self):
        """Test post view with missing fields."""
        post_args = (
            f'/user/-/signup/complete/{self.uid}/{self.token}',
            {'password1': 'superstrongpassword'}
        )
        response = self.client.post(*post_args)
        self.assertEqual(200, response.status_code)
        self.assertEqual('Missing fields', response.context.get('error'))

    @mock.patch('datawarehouse.accounts.views.validate_password')
    def test_post_weak_password(self, validate_password):
        """Test post view with weak passwords."""
        validate_password.side_effect = ValidationError(['Error 1.', 'Error 2.'])
        post_args = (
            f'/user/-/signup/complete/{self.uid}/{self.token}',
            {'password1': 'foo', 'password2': 'foo'}
        )
        response = self.client.post(*post_args)
        self.assertEqual(200, response.status_code)
        self.assertEqual('Error 1. Error 2.', response.context.get('error'))

    def test_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['get', 'post']
        url = f'/user/-/signup/complete/{self.uid}/{self.token}'

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            response = getattr(self.client, method)(url)
            self.assertEqual(405, response.status_code, method)


class UserSubscriptionsTest(utils.TestCase):
    """Test user_subscriptions."""

    fixtures = [
        'tests/accounts/fixtures/basic.yaml',
    ]

    def setUp(self):
        """SetUp."""
        self.user = User.objects.get(username='loyal_user')
        self.auth_client = self.get_authenticated_client(self.user)

    @freeze_time("2010-01-02 09:00:00")
    def test_subscribe(self):
        """Test subscribing user."""
        post_args = ('/user/-/subscriptions',
                     {'subscriptions': ['issue_regression'],
                      'issue_regression_visibility': 'cc'})
        post_kwargs = {
            'redirect_login': True,
            'redirect_url': '/user/-/login/?next=/user/-/subscriptions',
            'user': self.user,
        }
        self.assert_authenticated_post(302, [], *post_args, **post_kwargs)
        self.assertTrue(self.user.subscriptions.issue_regression)
        self.assertEqual(
            self.user.subscriptions.issue_regression_subscribed_at,
            timestamp_to_datetime('2010-01-02T09:00:00+00:00')
        )
        self.assertEqual(
            self.user.subscriptions.issue_regression_visibility, 'cc'
        )

    @freeze_time("2010-01-02 09:00:00")
    def test_subscribe_bcc(self):
        """Test subscribing user, as BCC."""
        post_args = ('/user/-/subscriptions',
                     {'subscriptions': ['issue_regression'],
                      'issue_regression_visibility': 'bcc'})
        post_kwargs = {
            'redirect_login': True,
            'redirect_url': '/user/-/login/?next=/user/-/subscriptions',
            'user': self.user,
        }
        self.assert_authenticated_post(302, [], *post_args, **post_kwargs)
        self.assertTrue(self.user.subscriptions.issue_regression)
        self.assertEqual(
            self.user.subscriptions.issue_regression_subscribed_at,
            timestamp_to_datetime('2010-01-02T09:00:00+00:00')
        )
        self.assertEqual(
            self.user.subscriptions.issue_regression_visibility, 'bcc'
        )

    def test_subscribe_noise(self):
        """Test subscribing user to incorrect things."""
        post_args = ('/user/-/subscriptions',
                     {'subscriptions': ['foo', 'bar']})
        post_kwargs = {
            'redirect_login': True,
            'redirect_url': '/user/-/login/?next=/user/-/subscriptions',
            'user': self.user,
        }
        self.assert_authenticated_post(302, [], *post_args, **post_kwargs)
        self.assertFalse(self.user.subscriptions.issue_regression)

    def test_unsubscribe(self):
        """Test unsubscribing user."""
        post_args = ('/user/-/subscriptions',
                     {'subscriptions': ['issue_regression']})
        post_kwargs = {
            'redirect_login': True,
            'redirect_url': '/user/-/login/?next=/user/-/subscriptions',
            'user': self.user,
        }
        self.assert_authenticated_post(302, [], *post_args, **post_kwargs)
        self.assertTrue(self.user.subscriptions.issue_regression)

        post_args = ('/user/-/subscriptions',
                     {'subscriptions': []})
        self.assert_authenticated_post(302, [], *post_args, **post_kwargs)
        self.user.subscriptions.refresh_from_db()
        self.assertFalse(self.user.subscriptions.issue_regression)
        self.assertIsNone(self.user.subscriptions.issue_regression_subscribed_at)

    def test_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['post']
        url = '/user/-/subscriptions'

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            response = getattr(self.auth_client, method)(url)
            self.assertEqual(405, response.status_code, method)

"""Test kcidb serializers."""
from datawarehouse import models
from datawarehouse import serializers
from tests import utils


class TestIssueSerializer(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Test Issue Serializer serializers."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issue_with_kcidb.yaml',
    ]

    def test_first_seen(self):
        """Check Issue's first_seen property."""
        checkout = models.KCIDBCheckout.objects.last()
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertEqual(
                serialized['first_seen'],
                checkout.start_time
            )

    def test_first_seen_no_revision(self):
        """Check Issue's first_seen property when there's no checkout."""
        models.KCIDBCheckout.objects.all().delete()
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertIsNone(
                serialized['first_seen'],
            )

    def test_first_seen_no_start_time(self):
        """Check Issue's first_seen property when there's no start_time."""
        models.KCIDBCheckout.objects.update(start_time=None)
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertIsNone(
                serialized['first_seen'],
            )

    def test_last_seen(self):
        """Check Issue's last_seen property."""
        checkout = models.KCIDBCheckout.objects.first()
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertEqual(
                serialized['last_seen'],
                checkout.start_time
            )

    def test_last_seen_no_revision(self):
        """Check Issue's last_seen property when there's no checkout."""
        models.KCIDBCheckout.objects.all().delete()
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertIsNone(
                serialized['last_seen'],
            )

    def test_last_seen_no_start_time(self):
        """Check Issue's last_seen property when there's no start_time."""
        models.KCIDBCheckout.objects.update(start_time=None)
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertIsNone(
                serialized['last_seen'],
            )

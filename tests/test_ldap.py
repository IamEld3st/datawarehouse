"""Test ldap module."""
from unittest import mock

from django.test import override_settings

from datawarehouse import ldap
from tests import utils


@override_settings(LDAP_CONFIG={'server_url': 'ldap.url', 'base_search': 'dc=foo,dc=bar',
                                'members_field': 'member', 'email_field': 'mail'})
class TestLDAP(utils.TestCase):
    """Test ldap module."""

    @mock.patch('datawarehouse.scripts.policies.ldap.ldap3.Connection', mock.Mock())
    def test_connection(self):
        """Test connection wrapper."""
        with ldap.connection() as conn:
            self.assertTrue(isinstance(conn, ldap.LDAPConnection))

    @mock.patch('datawarehouse.scripts.policies.ldap.ldap3.Connection')
    def test_connect(self, mock_connection):
        """Test connect method."""
        conn = ldap.LDAPConnection()

        conn.connect()

        self.assertEqual(conn.connection, mock_connection())
        mock_connection.assert_has_calls([
            mock.call().bind()
        ])

    @mock.patch('datawarehouse.scripts.policies.ldap.ldap3.Connection')
    def test_disconnect(self, mock_connection):
        """Test disconnect method."""
        conn = ldap.LDAPConnection()

        conn.connect()
        self.assertEqual(conn.connection, mock_connection())

        conn.disconnect()
        mock_connection.assert_has_calls([
            mock.call().bind(),
            mock.call().unbind(),
        ])
        self.assertIsNone(conn.connection)

    @mock.patch('datawarehouse.scripts.policies.ldap.ldap3.Connection')
    def test_get_users(self, mock_connection):
        """Test get users."""
        conn = ldap.LDAPConnection()
        conn.connect()
        conn.connection.entries = """
            , DN: uid=user_1,ou=users,dc=redhat,dc=com - STATUS: Read - READ TIME:
                mail: user_1@redhat.com
            , DN: uid=user_2,ou=users,dc=redhat,dc=com - STATUS: Read - READ TIME:
                mail: user_2@redhat.com
        """
        self.assertEqual(
            [
                ('user_1', 'user_1@redhat.com'),
                ('user_2', 'user_2@redhat.com')
            ],
            conn.get_users('cn=somegroup,ou=adhoc,ou=managedGroups,dc=foo,dc=bar')
        )
        mock_connection.assert_has_calls([
            mock.call().search(
                search_base='dc=foo,dc=bar',
                search_filter=(
                    '(&(objectClass=person)'
                    '(memberOf=cn=somegroup,ou=adhoc,ou=managedGroups,dc=foo,dc=bar))'
                ),
                search_scope='SUBTREE',
                attributes=['member', 'mail'],
                size_limit=0
            ),
        ])

"""Test the scripts.misc module."""
import datetime
from unittest import mock

from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from datawarehouse.scripts import misc
from tests import utils


class ScriptsMiscTest(utils.TestCase):
    """Unit tests for the scripts.misc module."""

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification')
    def test_send_kcidb_objects_for_retriage(self, send_notif):
        """Test send_kcidb_object_for_retriage."""
        def get_date(days_ago):
            """Get timestamp from {days_ago}."""
            return timezone.now() - datetime.timedelta(days=days_ago)

        origin = models.KCIDBOrigin.objects.create(name='redhat')

        checkout_1 = models.KCIDBCheckout.objects.create(
            origin=origin, id='rev1',
            start_time=get_date(2),
            valid=False
        )
        checkout_2 = models.KCIDBCheckout.objects.create(
            origin=origin, id='rev2',
            start_time=get_date(2),
            valid=True,
        )
        checkout_3 = models.KCIDBCheckout.objects.create(
            origin=origin, id='rev3',
            start_time=get_date(4),
            valid=False,
        )
        build_1 = models.KCIDBBuild.objects.create(
            origin=origin, checkout=checkout_1, id='redhat:build-1',
            valid=False
        )
        build_2 = models.KCIDBBuild.objects.create(
            origin=origin, checkout=checkout_2, id='redhat:build-2',
            valid=True
        )
        build_3 = models.KCIDBBuild.objects.create(
            origin=origin, checkout=checkout_3, id='redhat:build-3',
            valid=False
        )
        test_1 = models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-1',
            status=models.ResultEnum.FAIL,
        )
        test_2 = models.KCIDBTest.objects.create(
            origin=origin, build=build_2, id='redhat:test-2',
            status=models.ResultEnum.FAIL,
        )
        test_3 = models.KCIDBTest.objects.create(
            origin=origin, build=build_3, id='redhat:test-3',
            status=models.ResultEnum.FAIL,
        )
        test_4 = models.KCIDBTest.objects.create(
            origin=origin, build=build_2, id='redhat:test-4',
            status=models.ResultEnum.PASS,
        )
        models.KCIDBTestResult.objects.create(  # Not retriaged: PASS
            test=test_1, id='redhat:test-1.1',
            status=models.ResultEnum.PASS,
        )
        models.KCIDBTestResult.objects.create(  # Not retriaged: Old
            test=test_3, id='redhat:test-3.1',
            status=models.ResultEnum.FAIL,
        )
        testresult_1 = models.KCIDBTestResult.objects.create(
            test=test_4, id='redhat:test-4.1',
            status=models.ResultEnum.FAIL,
        )
        issue = models.Issue.objects.create(
            description='test issue',
            ticket_url='http://ticket',
            kind=models.IssueKind.objects.create(tag='foo', description='foo')
        )
        issue_regex_1 = models.IssueRegex.objects.create(
            issue=issue, text_match='text match'
        )
        issue_regex_2 = models.IssueRegex.objects.create(
            issue=issue, text_match='text match 2'
        )

        misc.send_kcidb_object_for_retriage([
            {'since_days_ago': 3, 'issueregex_id': 999},  # Missing id
        ])
        self.assertFalse(send_notif.called)

        misc.send_kcidb_object_for_retriage([
            {'since_days_ago': 3, 'issueregex_id': issue_regex_1.id},
            {'since_days_ago': 3, 'issueregex_id': issue_regex_2.id},
            {'since_days_ago': 3, 'issueregex_id': issue_regex_1.id},
            {'since_days_ago': 3, 'issueregex_id': 999},  # Missing id
        ])
        send_notif.assert_has_calls(
            [
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'checkout',
                    'object': serializers.KCIDBCheckoutSerializer(checkout_1).data,
                    'id': checkout_1.id,
                    'iid': checkout_1.iid,
                    'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                }]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'build',
                    'object': serializers.KCIDBBuildSerializer(build_1).data,
                    'id': build_1.id,
                    'iid': build_1.iid,
                    'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                }]),
                mock.call([
                    {
                        'timestamp': '2010-01-02T09:00:00+00:00',
                        'status': 'needs_triage',
                        'object_type': 'test',
                        'object': serializers.KCIDBTestSerializer(test_1).data,
                        'id': test_1.id,
                        'iid': test_1.iid,
                        'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                    },
                    {
                        'timestamp': '2010-01-02T09:00:00+00:00',
                        'status': 'needs_triage',
                        'object_type': 'test',
                        'object': serializers.KCIDBTestSerializer(test_2).data,
                        'id': test_2.id,
                        'iid': test_2.iid,
                        'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                    }
                ]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'testresult',
                    'object': serializers.KCIDBTestResultSerializer(testresult_1).data,
                    'id': testresult_1.id,
                    'iid': testresult_1.iid,
                    'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                }]),
            ]
        )

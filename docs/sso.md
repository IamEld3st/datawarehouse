# Enabling SSO SAML2

It's possible to configure Datawarehouse to rely on a SAML2 IdP for logging in.
The [djangosaml2] library is used.

## Configuration

Certain values are necessary to be configured for SAML2 to work. The following
parameters are set by environment variables:

| Name           | Type   | Description                                 |
|----------------|--------|---------------------------------------------|
| `SAML_ENABLED` | `bool` | Enable/Disable SAML2.                       |
| `SAML_CONFIG`  | `yaml` | Data necessary for the SAML2 configuration. |

In addition to this, certain directories and files specified in the
configuration need to be mounted on the given locations.

### SAML_CONFIG

The following is an example of the required values in the `SAML_CONFIG`
variable:

```yaml
---
name: SSO Name

attribute_map_dir: /path/to/directory

metadata:
  remote:
    - url: https://your-idp-provider/metadata

signing:
  cert_path: /path/to/cert
  key_path: /path/to/key

encryption:
  cert_path: /path/to/cert
  key_path: /path/to/key

contact_person:
  - given_name: First Name
    sur_name: Last Name
    company: Company Name
    email_address: email@domain
    contact_type: technical, administrative, etc

organization:
  name: Organization Name
  display_name: Display name
  url: https://organization.url
```

- metadata: Information about the IdP.
  ([docs](https://pysaml2.readthedocs.io/en/latest/howto/config.html#metadata))
- signing: Pair of key + cert used for signing the metadata file.
  (docs [key](https://pysaml2.readthedocs.io/en/latest/howto/config.html#key-file),
  [cert](https://pysaml2.readthedocs.io/en/latest/howto/config.html#cert-file))
- encryption: Pair of key + cert used for encryption.
  ([docs](https://pysaml2.readthedocs.io/en/latest/howto/config.html#encryption-keypairs))
- contact_person: Describe who can be contacted.
  ([docs](https://pysaml2.readthedocs.io/en/latest/howto/config.html#contact-person))
- organization: Describe the organization responsible for the service.
  ([docs](https://pysaml2.readthedocs.io/en/latest/howto/config.html#organization))

### Generating keys and certificates

Two **different** pairs of key+certificate are needed: one for signing the
metadata and another one for encryption.

```bash
# Generate a 4096 bits key
openssl genrsa -out $NAME.key 4096
# Generate a certificate request for that key
openssl req -new -key $NAME.key -out $NAME.csr
# Generate the certificate
openssl x509 -req -days 365 -in $NAME.csr -signkey $NAME.key -out $NAME.pem
```

From the previous commands, we need to configure `$NAME.key` in the `key_path` field
and `$NAME.pem` in the `cert_path`.

### Attribute map dir

It's necessary to provide a mapping between the SAML fields and the app-known ones.
[djangosaml2] expects a directory where these mappings are placed.

An example of an attributes map is the following:

```python
MAP = {
    "identifier": "urn:oasis:names:tc:SAML:2.0:attrname-format:basic",
    "fro": {
        'urn:oid:1.2.840.113549.1.9.1': 'email',
        'urn:oid:2.5.4.42': 'givenName',
        'urn:oid:2.5.4.4': 'surname',
    }
}
```

The `attributes-map-dir` path can contain multiple map files.
More information in the [docs](https://pysaml2.readthedocs.io/en/latest/howto/config.html#attribute-map-dir).

[djangosaml2]: https://github.com/peppelinux/djangosaml2

# Caching Views

To improve responsiveness and reduce the load DataWarehouse supports caching
anonymous requests, which can be enabled with the `FF_CACHE_ANONYMOUS` flag.

By default, Django uses [local memory caching] if no other backend is
configured. This approach utilizes per-process memory to store the data.

For production environments, it is possible to use the `MEMCACHED_SERVER`
environment variable to configure the [Memcached backend].
The format expected is `HOST:PORT`.

## Example

```bash
export FF_CACHE_ANONYMOUS=true
export MEMCACHED_SERVER=localhost:11211
```

[local memory caching]: https://docs.djangoproject.com/en/3.2/topics/cache/#local-memory-caching
[Memcached backend]: https://docs.djangoproject.com/en/3.2/topics/cache/#memcached

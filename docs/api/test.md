# Test

## Get

Get a single test.

`GET /api/1/test/$test_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `test_id` | `int` | Yes | ID of the test to get. |

Example of response:

```json
{
    "id": 43,
    "name": "Loopdev Sanity",
    "fetch_url": "https://gitlab.com/cki-project/kernel-tests/-/archive/main/kernel-tests-main.zip#filesystems/loopdev/sanity",

    "maintainers": [
        {
            "name": "Mr. Red Hat",
            "email": "mrredhat@redhat.com"
        }
    ]
}
```

## List

List all the tests.

`GET /api/1/test`

Example of response:

```json

{
    "count": 1,
    "next": "http://server/api/1/test?limit=30&offset=30",
    "previous": null,
    "results": [
        {
            "id": 43,
            "name": "Loopdev Sanity",
            "fetch_url": "https://gitlab.com/cki-project/kernel-tests/-/archive/main/kernel-tests-main.zip#filesystems/loopdev/sanity",
            "maintainers": [
                {
                    "name": "Mr. Red Hat",
                    "email": "mrredhat@redhat.com"
                }
            ]
        }
    ]
}
```

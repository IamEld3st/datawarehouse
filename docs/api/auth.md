# Authentication

The authentication for API requests uses Django's
[TokenAuthentication]. This means that you can create a token and set
the `Authorization` header in your requests, like so:

```yaml
Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b
```

See the [Data Warehouse permissions] documentation for instructions on
how to request a token.

[Data Warehouse permissions]: /permissions.html
[TokenAuthentication]: https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication

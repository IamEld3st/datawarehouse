"""Urls file."""
from django.urls import include
from django.urls import path

from . import views

urlpatterns = [
    path('1/issue', views.IssueList.as_view()),
    path('1/issue/-/regex', views.IssueRegexList.as_view()),
    path('1/issue/-/regex/<int:issue_regex_id>', views.IssueRegexGet.as_view(),
         name='issue.regex.get'),
    path('1/issue/<int:issue_id>', views.IssueGet.as_view(), name='issue.get'),
    path('1/kcidb/', include('datawarehouse.api.kcidb.urls')),
    path('1/test', views.TestList.as_view()),
    path('1/test/<int:test_id>', views.TestSingle.as_view()),
]

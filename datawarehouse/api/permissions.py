"""Permissions module."""
from rest_framework import permissions

PERMISSION_METHOD = permissions.DjangoModelPermissions()


class DjangoModelPermissionOrReadOnly(permissions.BasePermission):
    """DjangoModelPermissions or ReadOnly."""

    def has_permission(self, request, view):
        """Define if the user has permission for the request."""
        request_is_safe = request.method in permissions.SAFE_METHODS
        user_has_permissions = PERMISSION_METHOD.has_permission(request, view)

        return request_is_safe or user_has_permissions

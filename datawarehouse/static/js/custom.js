function filterDropdown(originalFilter, selectElementId, elementsList) {
    const filter = originalFilter.toLowerCase();
    const dropdown = document.getElementById(selectElementId);

    // Remove all options first
    while (dropdown.firstChild)
        dropdown.removeChild(dropdown.firstChild);

    // Add the ones that contain the filter as a substring
    for (let element of elementsList) {
        if (
            element.tag.toLowerCase().indexOf(filter) !== -1 ||
                element.description.toLowerCase().indexOf(filter) !== -1
        ) {
            const option = document.createElement('option');
            option.value = element.id;
            option.innerText = element.description;
            dropdown.appendChild(option);
        }
    }
}

function confirmRemove(what){
    return confirm('Do you really want to remove this ' + what + '? This action can not be undone');
}

function confirmRemoveIssue(elem) {
    // Confirmation dialog to delete an Issue
    return confirmRemove('issue');
}

function confirmRemoveBuild(elem) {
    // Confirmation dialog to delete a Build from an Issue Occurrence.
    return confirmRemove('build from the issue');
}

function confirmRemoveTest(elem) {
    // Confirmation dialog to delete a Test from an Issue Occurrence.
    return confirmRemove('test from the issue');
}

function findGetParameter(parameterName) {
    // Return value of option in GET parameters
    let params = (new URL(document.location)).searchParams;
    return params.get(parameterName);
}

function setSelectedValue(selectElementId, valueToSet) {
  // Set the value of a dropdown select element
  var selectObj = document.getElementById(selectElementId);
  for (var i = 0; i < selectObj.options.length; i++) {
      if (selectObj.options[i].value == valueToSet) {
          selectObj.options[i].selected = true;
          return;
      }
  }
}

# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Misc scripts file."""
import datetime

from celery import shared_task
from cki_lib.logger import get_logger
from django.utils import timezone

from datawarehouse import models
from datawarehouse import pagination
from datawarehouse import signals

LOGGER = get_logger(__name__)


@shared_task
def send_kcidb_object_for_retriage(calls_kwargs):
    """
    Add last n days objects to the queue for triaging.

    If multiple calls are included in calls_kwargs, the maximum 'since_days_ago'
    value is selected. Defaults to 15.
    """
    since_days_ago = max(call.get('since_days_ago', 15) for call in calls_kwargs)
    date_from = timezone.now() - datetime.timedelta(days=since_days_ago)

    issueregex_ids = {
        call['issueregex_id'] for call in calls_kwargs
        # Make sure the regex still exists (was not deleted right after it was created)
        if models.IssueRegex.objects.filter(id=call['issueregex_id']).exists()
    }
    if not issueregex_ids:
        return

    checkouts = models.KCIDBCheckout.objects.filter(start_time__gte=date_from)
    to_retriage = {
        'checkout': checkouts.filter(valid=False),
        'build': models.KCIDBBuild.objects.filter(
            checkout__in=checkouts,
            valid=False,
        ),
        'test': models.KCIDBTest.objects.filter(
            build__checkout__in=checkouts,
            status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES,
        ),
        'testresult': models.KCIDBTestResult.objects.filter(
            test__build__checkout__in=checkouts,
            status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES,
        ),
    }

    for kind, objects in to_retriage.items():
        paginator = pagination.EndlessPaginator(objects, 100)
        page_number = 1
        while page := paginator.get_page(page_number):
            signals.kcidb_object.send(
                sender='scripts.misc.send_kcidb_object_for_retriage',
                status=models.ObjectStatusEnum.NEEDS_TRIAGE,
                object_type=kind,
                objects=page,
                misc={'issueregex_ids': list(issueregex_ids)},
            )
            page_number += 1

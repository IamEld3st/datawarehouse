"""Saml modules."""
from django.db import IntegrityError
import djangosaml2.backends

from datawarehouse.models import SAMLUser


class Saml2Backend(djangosaml2.backends.Saml2Backend):
    # pylint: disable=too-few-public-methods
    """Saml2Backend override."""

    def clean_attributes(self, attributes: dict, idp_entityid: str, **kwargs) -> dict:
        # pylint: disable=unused-argument, no-self-use
        """Override some attribues returned by SAML assertion. Placeholder."""
        return attributes

    def get_or_create_user(self, *args, **kwargs):
        """Override get_or_create_user."""
        user, created = super().get_or_create_user(*args, **kwargs)

        # Save the user in order to create the SAMLUser object.
        if created:
            user.save()

        # Create SAMLUser instance to identify this user as SAML user.
        try:
            SAMLUser.objects.create(user=user)
        except IntegrityError:
            # User already exists, no problem.
            pass

        return user, created

# Generated by Django 3.2.6 on 2021-08-25 08:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0063_samlgrouplink'),
    ]

    operations = [
        migrations.AddField(
            model_name='kcidbcheckout',
            name='policy',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='datawarehouse.policy'),
        ),
        migrations.RemoveField(
            model_name='gittree',
            name='policy',
        ),
    ]

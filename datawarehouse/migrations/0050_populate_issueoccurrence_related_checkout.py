"""Populate IssueOccurrence related_checkout."""
from django.db import migrations, models
from django.db.models import Count


def populate_checkouts(apps, schema_editor):
    """Populate related_checkout in IssueOccurrence."""
    IssueOccurrence = apps.get_model('datawarehouse', 'IssueOccurrence')
    db_alias = schema_editor.connection.alias

    issue_occurrences = (
        IssueOccurrence.objects.using(db_alias)
        .all()
        .select_related(
            'kcidb_checkout',
            'kcidb_build',
            'kcidb_build__checkout',
            'kcidb_test',
            'kcidb_test__build__checkout'
        )
    )

    for issue_occurrence in issue_occurrences:
        if issue_occurrence.kcidb_checkout:
            checkout = issue_occurrence.kcidb_checkout
        elif issue_occurrence.kcidb_build:
            checkout = issue_occurrence.kcidb_build.checkout
        else:
            checkout = issue_occurrence.kcidb_test.build.checkout

        issue_occurrence.related_checkout = checkout
        issue_occurrence.save()


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0049_issueoccurrence_related_checkout'),
    ]

    operations = [
        migrations.RunPython(populate_checkouts),
    ]

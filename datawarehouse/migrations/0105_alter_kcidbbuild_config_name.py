# Generated by Django 3.2.13 on 2022-07-04 08:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0104_provenancecomponent_service_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kcidbbuild',
            name='config_name',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]

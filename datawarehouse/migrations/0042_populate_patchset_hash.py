"""Populate patchset_hash."""
from django.db import migrations, models


def populate(apps, schema_editor):
    """Split patchset_hash when applicable."""
    KCIDBCheckout = apps.get_model('datawarehouse', 'KCIDBCheckout')
    db_alias = schema_editor.connection.alias

    for checkout in KCIDBCheckout.objects.using(db_alias).filter(id__contains='+'):
        checkout.patchset_hash = checkout.id.split('+')[1]
        checkout.save()


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0041_rename_discovery_time_kcidbcheckout_start_time'),
    ]

    operations = [
        migrations.RunPython(populate),
    ]

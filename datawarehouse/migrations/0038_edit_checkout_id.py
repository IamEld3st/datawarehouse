"""Edit checkout id."""
from django.db import migrations, models
from django.db.models import F, Value
from django.db.models.functions import Concat


def add_origin_to_checkout_id(apps, schema_editor):
    """Prepend the origin name to the checkouts."""
    KCIDBOrigin = apps.get_model('datawarehouse', 'KCIDBOrigin')
    KCIDBCheckout = apps.get_model('datawarehouse', 'KCIDBCheckout')
    db_alias = schema_editor.connection.alias

    for origin in KCIDBOrigin.objects.using(db_alias).all():
        KCIDBCheckout.objects.using(db_alias).filter(origin=origin).update(
            id=Concat(Value(origin.name), Value(':'), F('id'))
        )


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0037_create_log_excerpt'),
    ]

    operations = [
        migrations.RunPython(add_origin_to_checkout_id),
    ]

from django.db import migrations, models

class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0034_kcidbrevision_patchset_hash'),
    ]

    operations = [
        migrations.RenameModel('KCIDBRevision', 'KCIDBCheckout'),
        migrations.RenameField('KCIDBBuild', 'revision', 'checkout'),
        migrations.RenameField('GitlabJob', 'kcidb_revision', 'kcidb_checkout'),
        migrations.RenameField('Report', 'kcidb_revision', 'kcidb_checkout'),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='contacts',
            field=models.ManyToManyField(blank=True, related_name='checkouts', to='datawarehouse.Maintainer'),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='patches',
            field=models.ManyToManyField(blank=True, related_name='checkouts', to='datawarehouse.Patch'),
        ),
    ]

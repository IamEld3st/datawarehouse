"""Models for scheduled tasks."""
import datetime

from cki_lib.logger import get_logger
from django.db import models
from django.utils import timezone
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import celery
from datawarehouse.models.utils import Manager

LOGGER = get_logger(__name__)


class QueuedTaskManager(Manager):
    """QueuedTask manager."""

    def filter_ready_to_run(self):
        """Filter QueuedTask ready to run."""
        return self.filter(run_at__lte=timezone.now())


class QueuedTask(EMOM('queued_task'), models.Model):
    """Model for QueuedTask."""

    # Name matching celery task name
    name = models.CharField(max_length=500)
    # Arbitrary ID to identify unique calls
    call_id = models.CharField(max_length=200)
    # List of kwargs. Each time the task is called, the call kwargs are appended to the list.
    calls_kwargs = models.JSONField(default=list)
    run_at = models.DateTimeField()

    objects = QueuedTaskManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} [{self.call_id}]'

    @classmethod
    def create(cls, name, call_id, call_kwargs, *,
               run_in_minutes=5):
        """Create task."""
        run_at = timezone.now() + datetime.timedelta(minutes=run_in_minutes)
        task, created = cls.objects.get_or_create(
            name=name,
            call_id=call_id,
            defaults={
                'calls_kwargs': [call_kwargs],
                'run_at': run_at,
            }
        )

        if not created:
            task.run_at = run_at
            task.calls_kwargs.append(call_kwargs)
            task.save()

        LOGGER.info("QueuedTask: %s %s. Next run: %s",
                    task, 'Created' if created else 'Postponed', task.run_at)

        return task

    def run(self):
        """
        Run the scheduled task.

        The celery task is called with all the calls_kwargs stored for the
        task.
        """
        LOGGER.info("QueuedTask: Running %s", self)
        celery.app.signature(self.name)(self.calls_kwargs)
        self.delete()

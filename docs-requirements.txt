markdown
mkdocs
mkdocs-awesome-pages-plugin
mkdocs-material
pymdown-extensions

# Workaround: current dnf version 3.0.1 breaks with mkdocs-material
Jinja2>=3.1.2

git+https://github.com/linkchecker/linkchecker.git
